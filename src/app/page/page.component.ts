import { Component, OnInit, HostBinding } from '@angular/core';
import * as data from '../data/data.json';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  host: {'[id]': 'id'}
})
export class PageComponent implements OnInit {

  pages = data.pages;
  activePage;
  id;

  constructor(router: Router) {

    this.activePage = this.pages[0];

    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        let pageUrl = router.url.split('/article/')[1];

        if (!pageUrl) {
          this.activePage = this.pages[0];
        } else {
          this.pages.forEach((page, i) => {
            if (page.id === pageUrl) {
              this.activePage = this.pages[i];
              this.id = this.activePage.id;
            }
          });
        }
      }
    });
  }

  ngOnInit(): void {}
}
