import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { PageComponent } from './page/page.component';

export const routingConfiguration: ExtraOptions = {
  onSameUrlNavigation: 'reload',
};

const routes: Routes = [
  {
    path: 'article/:activePage',
    component: PageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
