import { Component, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import * as data from './data/data.json';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  title = 'ebridge-uloha';
  pages = data.pages;
  scrolledAmount: number;
  menuToggled = false;

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.scrolledAmount = window.scrollY
  }

  constructor( public router: Router){
    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        this.toggleMenu();
      }
    });
  }
  toggleMenu(){
    this.menuToggled = !this.menuToggled;
  }

}
